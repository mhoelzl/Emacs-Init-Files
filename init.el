;;; Common Lisp Compatibility
;;; =========================

;;; Some packages (e.g., Slime) seem to implicitly rely on the CL
;;; package being present.

(require 'cl)

;;; Ensure that we use Unicode
;;; ==========================

(set-language-environment "UTF-8")


;;; Housekeeping and Startup Tasks.
;;; ==============================

;;; Options to suppress the startup screen, to position the window,
;;; and to set up load paths.

;;; No splash screen
(setq inhibit-startup-screen t)

;;; Disable the toolbar and don't play a bell sound.
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(setq visible-bell t)

;;; Set the window dimensions and position.
(when (and window-system (boundp initial-frame-alist))
  (setq initial-frame-alist
          '((top . 0) ; (left . 180)
            (width . 100) (height . 54))))

;;; Remember previously used files.
(require 'recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 50)

;;; Always highlight parentheses.
(when (fboundp 'show-paren-mode)
  (show-paren-mode))

;;; Set Default Coding Systems to UTF8
;;; ----------------------------------

(prefer-coding-system 'utf-8)


;;; Configuration Directories.
;;; -------------------------
  
(defvar user-config-directory
  (expand-file-name "config" user-emacs-directory)
  "The directory in which user-specific configuration files are
stored.")

(add-to-list 'load-path user-config-directory)

;;; Customizations should go into their own file.
(when (boundp custom-file)
  (setq custom-file
	(expand-file-name "custom.el" user-emacs-directory))
  (load custom-file))

;;; MELPA.
;;; -----

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

;;; El-get.
;;; ------

;; (add-to-list 'load-path
;; 	     (expand-file-name "el-get/el-get"
;; 			       user-emacs-directory))

;; (unless (require 'el-get nil 'noerror)
;;   (with-current-buffer
;;       (url-retrieve-synchronously
;;        "https://raw.githubusercontent.com/dimitri/el-get/master/el-get-install.el")
;;     (goto-char (point-max))
;;     (eval-print-last-sexp)))

;; (add-to-list 'el-get-recipe-path
;; 	     (expand-file-name "el-get-user/recipes"
;; 			       user-emacs-directory))
;; (el-get 'sync)

;; (when (boundp el-get-user-package-directory)
;;   (setq el-get-user-package-directory
;; 	(expand-file-name "el-get-init-files"
;; 			  user-emacs-directory)))


(message "Loading user configuration files.")

(dolist (init-file (directory-files user-config-directory t "\\.el$"))
  (load init-file))
(put 'downcase-region 'disabled nil)
