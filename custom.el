(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (org string-inflection slime markdown-mode julia-shell irony ess auctex)))
 '(safe-local-variable-values
   (quote
    ((TeX-master . awareness-based-ensembles\.tex)
     (TeX-master . awareness-based-ensembles)
     (TeX-view-style
      ("." "xpdf %s.pdf"))
     (TeX-master . \.\./fomac)
     (TeX-master . \.\./fomac\.tex)
     (Package . snark-feature)
     (Package . snark)
     (Syntax . Common-Lisp)
     (TeX-view-style
      ("." "evince %s.pdf"))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
