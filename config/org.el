;;; Emacs configuration for Org mode
;;; ================================

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(require 'ob-C)
(require 'ob-emacs-lisp)
(require 'ob-lisp)

;; (org-babel-do-load-languages
;;  'org-babel-load-languages
;;  '((C . t)
;;    (emacs-lisp . t)
;;    ;; (julia . t)
;;    (lisp . t)))
