(require 'ess-site)
(setq inferior-julia-program-name "julia")
(setq inferior-julia-args "-i --color=no")
(setq ess-use-auto-complete t)
