(require 'slime-autoloads)

(setq inferior-lisp-program "sbcl")
(setq slime-contribs '(slime-fancy))
(setq-default slime-net-coding-system 'utf-8-unix)
