(message "Loading auctex customizations.")

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-PDF-mode t)

(require 'reftex)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(add-hook 'latex-mode-hook 'turn-on-reftex)

(setq TeX-source-correlate-mode t)
(setq TeX-source-correlate-method 'synctex)
(when (or (eq system-type 'ms-dos)
	  (eq system-type 'windows-nt))
  (setq TeX-view-program-list
	'(("Sumatra PDF" ("\"C:/Program Files (x86)/SumatraPDF/SumatraPDF.exe\" -reuse-instance"
			  (mode-io-correlate " -forward-search %b %n ") " %o"))))
  (eval-after-load 'tex
    '(progn
       (assq-delete-all 'output-pdf TeX-view-program-selection)
       (add-to-list 'TeX-view-program-selection '(output-pdf "Sumatra PDF")))))

